#!/bin/bash

jupyter nbconvert "Cohort_Example.ipynb" --to rst
sed -i '1s/^/.. _cohort_example:\n\n/' Cohort_Example.rst
sed -i 's/\(.*\)\(Cohort_Example_\d*\)/.. image:: tutorials\/cohort\/\2/g' Cohort_Example.rst
rm ../docs/tutorials/cohort/*.png
mv Cohort_Example.rst ../docs/tutorials/cohort
mv Cohort_Example_files/*.png ../docs/tutorials/cohort
rm -d Cohort_Example_files

jupyter nbconvert "Multi_Cohort_Example.ipynb" --to rst
sed -i '1s/^/.. _multi_cohort_example:\n\n/' Multi_Cohort_Example.rst
sed -i 's/\(.*\)\(Multi_Cohort_Example_\d*\)/.. image:: tutorials\/multicohort\/\2/g' Multi_Cohort_Example.rst
rm ../docs/tutorials/multi_cohort/*.png
mv Multi_Cohort_Example.rst ../docs/tutorials/multi_cohort
mv Multi_Cohort_Example_files/*.png ../docs/tutorials/multi_cohort
rm -d Multi_Cohort_Example_files

jupyter nbconvert "Retention_Example.ipynb" --to rst
sed -i '1s/^/.. _retention_example:\n\n/' Retention_Example.rst
sed -i 's/\(.*\)\(Retention_Example_\d*\)/.. image:: tutorials\/retention\/\2/g' Retention_Example.rst
rm ../docs/tutorials/retention/*.png
mv Retention_Example.rst ../docs/tutorials/retention
mv Retention_Example_files/*.png ../docs/tutorials/retention
rm -d Retention_Example_files

jupyter nbconvert "Revenue_Example.ipynb" --to rst
sed -i '1s/^/.. _revenue_example:\n\n/' Revenue_Example.rst
sed -i 's/\(.*\)\(Revenue_Example_\d*\)/.. image:: tutorials\/revenue\/\2/g' Revenue_Example.rst
rm ../docs/tutorials/revenue/*.png
mv Revenue_Example.rst ../docs/tutorials/revenue
mv Revenue_Example_files/*.png ../docs/tutorials/revenue
rm -d Revenue_Example_files

