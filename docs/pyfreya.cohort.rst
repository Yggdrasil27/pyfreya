pyfreya.cohort package
======================

Submodules
----------

pyfreya.cohort.cohort module
----------------------------

.. automodule:: pyfreya.cohort.cohort
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: pyfreya.cohort
    :members:
    :undoc-members:
    :show-inheritance:
