.. _cohort_example:

Short Tutorial in the *Cohort* Class.
-------------------------------------

Retention
~~~~~~~~~

Let’s import the class and see insert a some retention numbers along
with the amount of new users in the cohort.

To get more info on retenion see `retention
tutorial. <https://pyfreya.readthedocs.io/en/latest/tutorials.html#short-tutorial-in-the-cohort-class>`__

.. code:: ipython3

    from pyfreya import Cohort
    
    new_users = 100
    days_since_install = [1, 7, 30]
    retention_values= [50, 15, 5]
    facebook = Cohort(new_users, days_since_install, retention_values, name='facebook')
    facebook




.. parsed-literal::

                            1
    DaysSinceInstall         
    0                     100
    1                 50.0629
    2                 32.1914
    3                 24.8632
    4                 20.6996
    5                 17.9566
    6                 15.9875
    7                 14.4921
    8                 13.3102
    9                  12.348
    10                11.5464
    11                10.8662
    12                10.2802
    13                9.76917
    14                9.31867
    15                8.91795
    16                8.55872
    17                8.23447
    18                7.94001
    19                7.67118
    20                7.42456
    21                7.19733
    22                6.98716
    23                6.79206
    24                6.61038
    25                6.44069
    26                6.28175
    27                6.13252
    28                5.99207
    29                 5.8596
    30                 5.7344
    31                5.61586



**Note:** That the cohort class can also take a retention profile
instead of actual retention data points. The name given is not of any
particular importance now, but when plotting various aggregates from
multiple cohorts easily identifiable names are nice to have - is no name
given a random one will be applied.

Daily Active Users
~~~~~~~~~~~~~~~~~~

Maybe similar cohorts comes in multiple days in a row. It is moddeled
like this:

.. code:: ipython3

    facebook.replicate_cohort(10)
    facebook # Also accessible as facebook.df_user_dist as a pandas DataFrame




.. parsed-literal::

                           1        2        3        4        5        6        7        8        9        10
    DaysSinceInstall                                                                                          
    0                     100      100      100      100      100      100      100      100      100      100
    1                 50.0629  50.0629  50.0629  50.0629  50.0629  50.0629  50.0629  50.0629  50.0629  50.0629
    2                     NaN  32.1914  32.1914  32.1914  32.1914  32.1914  32.1914  32.1914  32.1914  32.1914
    3                     NaN      NaN  24.8632  24.8632  24.8632  24.8632  24.8632  24.8632  24.8632  24.8632
    4                     NaN      NaN      NaN  20.6996  20.6996  20.6996  20.6996  20.6996  20.6996  20.6996
    5                     NaN      NaN      NaN      NaN  17.9566  17.9566  17.9566  17.9566  17.9566  17.9566
    6                     NaN      NaN      NaN      NaN      NaN  15.9875  15.9875  15.9875  15.9875  15.9875
    7                     NaN      NaN      NaN      NaN      NaN      NaN  14.4921  14.4921  14.4921  14.4921
    8                     NaN      NaN      NaN      NaN      NaN      NaN      NaN  13.3102  13.3102  13.3102
    9                     NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN   12.348   12.348
    10                    NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN      NaN  11.5464



Well - its nice to see this user distribution, but how many daily active
users do we have ? (also note the type is a pandas DataFrame)

.. code:: ipython3

    print(type(facebook.df_dau))
    facebook.df_dau


.. parsed-literal::

    <class 'pandas.core.frame.DataFrame'>




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>dau</th>
        </tr>
        <tr>
          <th>Date</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>100</td>
        </tr>
        <tr>
          <th>2</th>
          <td>150.063</td>
        </tr>
        <tr>
          <th>3</th>
          <td>182.254</td>
        </tr>
        <tr>
          <th>4</th>
          <td>207.117</td>
        </tr>
        <tr>
          <th>5</th>
          <td>227.817</td>
        </tr>
        <tr>
          <th>6</th>
          <td>245.774</td>
        </tr>
        <tr>
          <th>7</th>
          <td>261.761</td>
        </tr>
        <tr>
          <th>8</th>
          <td>276.253</td>
        </tr>
        <tr>
          <th>9</th>
          <td>289.563</td>
        </tr>
        <tr>
          <th>10</th>
          <td>301.912</td>
        </tr>
      </tbody>
    </table>
    </div>



Since users are still active after the influx of 10 days lets see what
it looks like after 30 days (10 days of user influx and 20 days of
waiting):

.. code:: ipython3

    facebook.replicate_cohort(10, 20)
    facebook.df_dau




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>dau</th>
        </tr>
        <tr>
          <th>Date</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>100</td>
        </tr>
        <tr>
          <th>2</th>
          <td>150.063</td>
        </tr>
        <tr>
          <th>3</th>
          <td>182.254</td>
        </tr>
        <tr>
          <th>4</th>
          <td>207.117</td>
        </tr>
        <tr>
          <th>5</th>
          <td>227.817</td>
        </tr>
        <tr>
          <th>6</th>
          <td>245.774</td>
        </tr>
        <tr>
          <th>7</th>
          <td>261.761</td>
        </tr>
        <tr>
          <th>8</th>
          <td>276.253</td>
        </tr>
        <tr>
          <th>9</th>
          <td>289.563</td>
        </tr>
        <tr>
          <th>10</th>
          <td>301.912</td>
        </tr>
        <tr>
          <th>11</th>
          <td>213.458</td>
        </tr>
        <tr>
          <th>12</th>
          <td>174.261</td>
        </tr>
        <tr>
          <th>13</th>
          <td>152.35</td>
        </tr>
        <tr>
          <th>14</th>
          <td>137.256</td>
        </tr>
        <tr>
          <th>15</th>
          <td>125.875</td>
        </tr>
        <tr>
          <th>16</th>
          <td>116.836</td>
        </tr>
        <tr>
          <th>17</th>
          <td>109.408</td>
        </tr>
        <tr>
          <th>18</th>
          <td>103.15</td>
        </tr>
        <tr>
          <th>19</th>
          <td>97.7799</td>
        </tr>
        <tr>
          <th>20</th>
          <td>93.103</td>
        </tr>
        <tr>
          <th>21</th>
          <td>88.9812</td>
        </tr>
        <tr>
          <th>22</th>
          <td>85.3123</td>
        </tr>
        <tr>
          <th>23</th>
          <td>82.0192</td>
        </tr>
        <tr>
          <th>24</th>
          <td>79.0421</td>
        </tr>
        <tr>
          <th>25</th>
          <td>76.3338</td>
        </tr>
        <tr>
          <th>26</th>
          <td>73.8566</td>
        </tr>
        <tr>
          <th>27</th>
          <td>71.5796</td>
        </tr>
        <tr>
          <th>28</th>
          <td>69.4776</td>
        </tr>
        <tr>
          <th>29</th>
          <td>67.5297</td>
        </tr>
        <tr>
          <th>30</th>
          <td>65.7181</td>
        </tr>
      </tbody>
    </table>
    </div>



Enough numbers, lets plot some of this. First, lets plot the retention -
maybe it fitted the data incorrectly:

.. code:: ipython3

    facebook.plot_retention()



.. image:: tutorials/cohort/Cohort_Example_9_0.png


How about dau?

.. code:: ipython3

    facebook.plot_dau()



.. image:: tutorials/cohort/Cohort_Example_11_0.png


If you wonder how long time it takes to reach a certain amount of dau it
can be calculated. This does assume a steady influx of users given in
*new_users* and with the retention profile calculated earlier.

.. code:: ipython3

    facebook.days_to_dau(400)




.. parsed-literal::

    21



.. code:: ipython3

    facebook.replicate_cohort(23)
    facebook.plot_dau()



.. image:: tutorials/cohort/Cohort_Example_14_0.png


Datetime
~~~~~~~~

What kind of date is this anyway? Lets use proper human dates from the
Gregorian calendar:

.. code:: ipython3

    from datetime import datetime
    
    start_date = datetime.strptime('2019-03-14', '%Y-%m-%d')
    facebook.start_date = start_date
    facebook.replicate_cohort(23) # we need to recalculate user distribution
    facebook.plot_dau()



.. image:: tutorials/cohort/Cohort_Example_16_0.png


Revenue
~~~~~~~

Well how much money did we earn? A premade revenue profile class called
``ARPDAU`` is imported and is initialized by setting the ARPDAU to a
value.

.. code:: ipython3

    from pyfreya.revenue import ARPDAU
    
    facebook.replicate_cohort(23, 10)
    revenue_profile = ARPDAU(2.1)
    facebook.revenue_profile = revenue_profile
    facebook.apply_revenue()
    facebook.df_dau




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>dau</th>
          <th>revenue</th>
        </tr>
        <tr>
          <th>Date</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>2019-03-14</th>
          <td>100</td>
          <td>210</td>
        </tr>
        <tr>
          <th>2019-03-15</th>
          <td>150.063</td>
          <td>315.132</td>
        </tr>
        <tr>
          <th>2019-03-16</th>
          <td>182.254</td>
          <td>382.734</td>
        </tr>
        <tr>
          <th>2019-03-17</th>
          <td>207.117</td>
          <td>434.947</td>
        </tr>
        <tr>
          <th>2019-03-18</th>
          <td>227.817</td>
          <td>478.416</td>
        </tr>
        <tr>
          <th>2019-03-19</th>
          <td>245.774</td>
          <td>516.125</td>
        </tr>
        <tr>
          <th>2019-03-20</th>
          <td>261.761</td>
          <td>549.698</td>
        </tr>
        <tr>
          <th>2019-03-21</th>
          <td>276.253</td>
          <td>580.132</td>
        </tr>
        <tr>
          <th>2019-03-22</th>
          <td>289.563</td>
          <td>608.083</td>
        </tr>
        <tr>
          <th>2019-03-23</th>
          <td>301.912</td>
          <td>634.014</td>
        </tr>
        <tr>
          <th>2019-03-24</th>
          <td>313.458</td>
          <td>658.262</td>
        </tr>
        <tr>
          <th>2019-03-25</th>
          <td>324.324</td>
          <td>681.081</td>
        </tr>
        <tr>
          <th>2019-03-26</th>
          <td>334.604</td>
          <td>702.669</td>
        </tr>
        <tr>
          <th>2019-03-27</th>
          <td>344.374</td>
          <td>723.184</td>
        </tr>
        <tr>
          <th>2019-03-28</th>
          <td>353.692</td>
          <td>742.754</td>
        </tr>
        <tr>
          <th>2019-03-29</th>
          <td>362.61</td>
          <td>761.481</td>
        </tr>
        <tr>
          <th>2019-03-30</th>
          <td>371.169</td>
          <td>779.455</td>
        </tr>
        <tr>
          <th>2019-03-31</th>
          <td>379.403</td>
          <td>796.747</td>
        </tr>
        <tr>
          <th>2019-04-01</th>
          <td>387.343</td>
          <td>813.421</td>
        </tr>
        <tr>
          <th>2019-04-02</th>
          <td>395.015</td>
          <td>829.531</td>
        </tr>
        <tr>
          <th>2019-04-03</th>
          <td>402.439</td>
          <td>845.122</td>
        </tr>
        <tr>
          <th>2019-04-04</th>
          <td>409.636</td>
          <td>860.237</td>
        </tr>
        <tr>
          <th>2019-04-05</th>
          <td>416.624</td>
          <td>874.91</td>
        </tr>
        <tr>
          <th>2019-04-06</th>
          <td>323.416</td>
          <td>679.173</td>
        </tr>
        <tr>
          <th>2019-04-07</th>
          <td>279.963</td>
          <td>587.923</td>
        </tr>
        <tr>
          <th>2019-04-08</th>
          <td>254.212</td>
          <td>533.846</td>
        </tr>
        <tr>
          <th>2019-04-09</th>
          <td>235.631</td>
          <td>494.825</td>
        </tr>
        <tr>
          <th>2019-04-10</th>
          <td>221.064</td>
          <td>464.234</td>
        </tr>
        <tr>
          <th>2019-04-11</th>
          <td>209.099</td>
          <td>439.109</td>
        </tr>
        <tr>
          <th>2019-04-12</th>
          <td>198.971</td>
          <td>417.84</td>
        </tr>
        <tr>
          <th>2019-04-13</th>
          <td>190.214</td>
          <td>399.449</td>
        </tr>
        <tr>
          <th>2019-04-14</th>
          <td>182.519</td>
          <td>383.291</td>
        </tr>
        <tr>
          <th>2019-04-15</th>
          <td>175.675</td>
          <td>368.917</td>
        </tr>
      </tbody>
    </table>
    </div>



This can be plotted too!

.. code:: ipython3

    facebook.plot_revenue()



.. image:: tutorials/cohort/Cohort_Example_20_0.png


If we are interested in uncertainties the
`Uncertainties <https://pypi.org/project/uncertainties/>`__ package have
been implemented. This can be used the following way:

.. code:: ipython3

    from pyfreya import ufloat
    
    retention_values= [ufloat(50, 3), ufloat(15, 1), ufloat(5, 0.5)]
    days_since_install = [1, 7, 30] # no uncertainties here.
    new_users = ufloat(100, 5)
    facebook = Cohort(new_users, days_since_install, retention_values)
    facebook.revenue_profile = ARPDAU(ufloat(2.1, 0.2))
    facebook.replicate_cohort(23, 10)
    facebook.apply_revenue()
    facebook.plot_retention()
    facebook.plot_dau()
    facebook.plot_revenue()



.. image:: tutorials/cohort/Cohort_Example_22_0.png



.. image:: tutorials/cohort/Cohort_Example_22_1.png



.. image:: tutorials/cohort/Cohort_Example_22_2.png


When working with uncertainties, the nominal values and the uncertainty
values can be obtained with functions ``nominal_values`` and
``std_devs``, respectively:

.. code:: ipython3

    from pyfreya import nominal_values, std_devs
    
    facebook.df_dau['dau']




.. parsed-literal::

    Date
    1      100+/-5
    2      151+/-8
    3     183+/-11
    4     208+/-12
    5     228+/-14
    6     246+/-15
    7     261+/-16
    8     275+/-17
    9     288+/-17
    10    300+/-18
    11    311+/-19
    12    322+/-20
    13    331+/-20
    14    341+/-21
    15    350+/-22
    16    358+/-22
    17    366+/-23
    18    374+/-23
    19    382+/-24
    20    389+/-24
    21    396+/-25
    22    403+/-26
    23    409+/-26
    24    316+/-23
    25    271+/-21
    26    245+/-19
    27    226+/-19
    28    211+/-18
    29    199+/-17
    30    189+/-17
    31    181+/-16
    32    173+/-16
    33    166+/-16
    Name: dau, dtype: object



.. code:: ipython3

    nominal_values(facebook.df_dau['dau'])




.. parsed-literal::

    array([100.        , 151.00796688, 183.23144399, 207.86303692,
           228.21970943, 245.77844367, 261.3390633 , 275.38876963,
           288.24877584, 300.14329757, 311.23575048, 321.64933796,
           331.47951354, 340.80190942, 349.6775894 , 358.15664906,
           366.28075478, 374.08497899, 381.59915514, 388.84889718,
           395.85637904, 402.64093973, 409.21955908, 315.60723627,
           270.8093275 , 244.6301836 , 225.88786696, 211.27502302,
           199.32335936, 189.24093946, 180.54774509, 172.92912575,
           166.16687945])



.. code:: ipython3

    std_devs(facebook.df_dau['dau'])




.. parsed-literal::

    array([ 5.        ,  8.34935123, 10.56825389, 12.20066331, 13.51786087,
           14.6418741 , 15.63646161, 16.53872917, 17.37200759, 18.15183883,
           18.88905055, 19.59146105, 20.26488384, 20.91374916, 21.54150443,
           22.1508812 , 22.74407817, 23.32289008, 23.88880016, 24.44304807,
           24.98668034, 25.52058868, 26.04553928, 22.86365271, 20.63415691,
           19.40634919, 18.5255104 , 17.82742722, 17.24464372, 16.74232945,
           16.29990806, 15.90409195, 15.54574024])



It is possible to save a cohort class instance (using *pickle*) and
loading it.

.. code:: python

   facebook.save('facebook_revenue.pkl')
   import pyfreya
   facebook_loaded = pyfreya.load('facebook_revenue.pkl')
