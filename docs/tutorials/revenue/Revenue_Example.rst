.. _revenue_example:

Short Tutorial in the *Revenue* Classes.
----------------------------------------

Let’s import the class and insert a some retention numbers along with
the amount of new users in a cohort. Additional information about
*Retention* and *Cohort* classes can be found at `retention
tutorial <https://pyfreya.readthedocs.io/en/latest/tutorials.html#>`__
and `cohort
tutorial <https://pyfreya.readthedocs.io/en/latest/tutorials.html#short-tutorial-in-the-cohort-class>`__.
Let’s also import a predefined revenue spending class (profile)
*ARPDAU*.

.. code:: ipython3

    from pyfreya import Cohort
    from pyfreya.revenue import ARPDAU
    
    new_users = 100
    days_since_install = [1, 7, 30]
    retention_values= [50, 15, 5]
    facebook = Cohort(new_users, days_since_install, retention_values)
    facebook




.. parsed-literal::

                            1
    DaysSinceInstall         
    0                     100
    1                 50.0629
    2                 32.1914
    3                 24.8632
    4                 20.6996
    5                 17.9566
    6                 15.9875
    7                 14.4921
    8                 13.3102
    9                  12.348
    10                11.5464
    11                10.8662
    12                10.2802
    13                9.76917
    14                9.31867
    15                8.91795
    16                8.55872
    17                8.23447
    18                7.94001
    19                7.67118
    20                7.42456
    21                7.19733
    22                6.98716
    23                6.79206
    24                6.61038
    25                6.44069
    26                6.28175
    27                6.13252
    28                5.99207
    29                 5.8596
    30                 5.7344
    31                5.61586



.. code:: ipython3

    facebook.revenue_profile = ARPDAU(2.1)
    facebook.apply_revenue()
    facebook.df_dau




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>dau</th>
          <th>revenue</th>
        </tr>
        <tr>
          <th>Date</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>100</td>
          <td>210</td>
        </tr>
        <tr>
          <th>2</th>
          <td>50.0629</td>
          <td>105.132</td>
        </tr>
        <tr>
          <th>3</th>
          <td>32.1914</td>
          <td>67.6019</td>
        </tr>
        <tr>
          <th>4</th>
          <td>24.8632</td>
          <td>52.2127</td>
        </tr>
        <tr>
          <th>5</th>
          <td>20.6996</td>
          <td>43.4692</td>
        </tr>
        <tr>
          <th>6</th>
          <td>17.9566</td>
          <td>37.7089</td>
        </tr>
        <tr>
          <th>7</th>
          <td>15.9875</td>
          <td>33.5737</td>
        </tr>
        <tr>
          <th>8</th>
          <td>14.4921</td>
          <td>30.4334</td>
        </tr>
        <tr>
          <th>9</th>
          <td>13.3102</td>
          <td>27.9515</td>
        </tr>
        <tr>
          <th>10</th>
          <td>12.348</td>
          <td>25.9309</td>
        </tr>
        <tr>
          <th>11</th>
          <td>11.5464</td>
          <td>24.2475</td>
        </tr>
        <tr>
          <th>12</th>
          <td>10.8662</td>
          <td>22.819</td>
        </tr>
        <tr>
          <th>13</th>
          <td>10.2802</td>
          <td>21.5885</td>
        </tr>
        <tr>
          <th>14</th>
          <td>9.76917</td>
          <td>20.5153</td>
        </tr>
        <tr>
          <th>15</th>
          <td>9.31867</td>
          <td>19.5692</td>
        </tr>
        <tr>
          <th>16</th>
          <td>8.91795</td>
          <td>18.7277</td>
        </tr>
        <tr>
          <th>17</th>
          <td>8.55872</td>
          <td>17.9733</td>
        </tr>
        <tr>
          <th>18</th>
          <td>8.23447</td>
          <td>17.2924</td>
        </tr>
        <tr>
          <th>19</th>
          <td>7.94001</td>
          <td>16.674</td>
        </tr>
        <tr>
          <th>20</th>
          <td>7.67118</td>
          <td>16.1095</td>
        </tr>
        <tr>
          <th>21</th>
          <td>7.42456</td>
          <td>15.5916</td>
        </tr>
        <tr>
          <th>22</th>
          <td>7.19733</td>
          <td>15.1144</td>
        </tr>
        <tr>
          <th>23</th>
          <td>6.98716</td>
          <td>14.673</td>
        </tr>
        <tr>
          <th>24</th>
          <td>6.79206</td>
          <td>14.2633</td>
        </tr>
        <tr>
          <th>25</th>
          <td>6.61038</td>
          <td>13.8818</td>
        </tr>
        <tr>
          <th>26</th>
          <td>6.44069</td>
          <td>13.5254</td>
        </tr>
        <tr>
          <th>27</th>
          <td>6.28175</td>
          <td>13.1917</td>
        </tr>
        <tr>
          <th>28</th>
          <td>6.13252</td>
          <td>12.8783</td>
        </tr>
        <tr>
          <th>29</th>
          <td>5.99207</td>
          <td>12.5833</td>
        </tr>
        <tr>
          <th>30</th>
          <td>5.8596</td>
          <td>12.3052</td>
        </tr>
        <tr>
          <th>31</th>
          <td>5.7344</td>
          <td>12.0422</td>
        </tr>
      </tbody>
    </table>
    </div>



As can be seen, there is also room for adding uncertainty. Let’s import
the base revenue class and define ARPDAU but with uncertainty. It does
make use of some properties of the ``Cohort`` class, it is recommended
to be a bit familiar with that.

.. code:: ipython3

    from pyfreya import BaseRevenue
    import numpy as np
    
    class ARPDAU2(BaseRevenue):
        r"""
        A basic revenue spending class. It applies an average revenue to each active users on a given
        date. There is no uncertainty applied to this class.
        """
    
        def __init__(self, arpdau=0):
            r"""
            Set the ARPDAU value.
    
            :param arpdau: Average revenue per daily active user.
            """
            self.arpdau = arpdau
    
        def _revenue(self, df_dau):
            r"""
            Add ARAPDAU to dau dataframe.
    
            :param df_dau: dau dataframe.
            :return: Revenue by date.
            """
            return df_dau['dau'] * self.arpdau
    
    facebook.revenue_profile = ARPDAU2(2.1)
    facebook.apply_revenue()
    facebook.df_dau




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }
    
        .dataframe tbody tr th {
            vertical-align: top;
        }
    
        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>dau</th>
          <th>revenue</th>
        </tr>
        <tr>
          <th>Date</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>100</td>
          <td>210</td>
        </tr>
        <tr>
          <th>2</th>
          <td>50.0629</td>
          <td>105.132</td>
        </tr>
        <tr>
          <th>3</th>
          <td>32.1914</td>
          <td>67.6019</td>
        </tr>
        <tr>
          <th>4</th>
          <td>24.8632</td>
          <td>52.2127</td>
        </tr>
        <tr>
          <th>5</th>
          <td>20.6996</td>
          <td>43.4692</td>
        </tr>
        <tr>
          <th>6</th>
          <td>17.9566</td>
          <td>37.7089</td>
        </tr>
        <tr>
          <th>7</th>
          <td>15.9875</td>
          <td>33.5737</td>
        </tr>
        <tr>
          <th>8</th>
          <td>14.4921</td>
          <td>30.4334</td>
        </tr>
        <tr>
          <th>9</th>
          <td>13.3102</td>
          <td>27.9515</td>
        </tr>
        <tr>
          <th>10</th>
          <td>12.348</td>
          <td>25.9309</td>
        </tr>
        <tr>
          <th>11</th>
          <td>11.5464</td>
          <td>24.2475</td>
        </tr>
        <tr>
          <th>12</th>
          <td>10.8662</td>
          <td>22.819</td>
        </tr>
        <tr>
          <th>13</th>
          <td>10.2802</td>
          <td>21.5885</td>
        </tr>
        <tr>
          <th>14</th>
          <td>9.76917</td>
          <td>20.5153</td>
        </tr>
        <tr>
          <th>15</th>
          <td>9.31867</td>
          <td>19.5692</td>
        </tr>
        <tr>
          <th>16</th>
          <td>8.91795</td>
          <td>18.7277</td>
        </tr>
        <tr>
          <th>17</th>
          <td>8.55872</td>
          <td>17.9733</td>
        </tr>
        <tr>
          <th>18</th>
          <td>8.23447</td>
          <td>17.2924</td>
        </tr>
        <tr>
          <th>19</th>
          <td>7.94001</td>
          <td>16.674</td>
        </tr>
        <tr>
          <th>20</th>
          <td>7.67118</td>
          <td>16.1095</td>
        </tr>
        <tr>
          <th>21</th>
          <td>7.42456</td>
          <td>15.5916</td>
        </tr>
        <tr>
          <th>22</th>
          <td>7.19733</td>
          <td>15.1144</td>
        </tr>
        <tr>
          <th>23</th>
          <td>6.98716</td>
          <td>14.673</td>
        </tr>
        <tr>
          <th>24</th>
          <td>6.79206</td>
          <td>14.2633</td>
        </tr>
        <tr>
          <th>25</th>
          <td>6.61038</td>
          <td>13.8818</td>
        </tr>
        <tr>
          <th>26</th>
          <td>6.44069</td>
          <td>13.5254</td>
        </tr>
        <tr>
          <th>27</th>
          <td>6.28175</td>
          <td>13.1917</td>
        </tr>
        <tr>
          <th>28</th>
          <td>6.13252</td>
          <td>12.8783</td>
        </tr>
        <tr>
          <th>29</th>
          <td>5.99207</td>
          <td>12.5833</td>
        </tr>
        <tr>
          <th>30</th>
          <td>5.8596</td>
          <td>12.3052</td>
        </tr>
        <tr>
          <th>31</th>
          <td>5.7344</td>
          <td>12.0422</td>
        </tr>
      </tbody>
    </table>
    </div>



.. code:: ipython3

    facebook.plot_revenue();



.. image:: tutorials/revenue/Revenue_Example_5_0.png


If we are interested in uncertainties the
`Uncertainties <https://pypi.org/project/uncertainties/>`__ package have
been implemented. This can be used the following way:

.. code:: ipython3

    from pyfreya import ufloat
    
    retention_values= [ufloat(50, 3), ufloat(15, 1), ufloat(5, 0.5)]
    days_since_install = [1, 7, 30] # no uncertainties here.
    new_users = ufloat(100, 5)
    facebook = Cohort(new_users, days_since_install, retention_values)
    facebook.revenue_profile = ARPDAU(ufloat(2.1, 0.2))
    facebook.apply_revenue()
    facebook.plot_revenue()



.. image:: tutorials/revenue/Revenue_Example_7_0.png


When working with uncertainties, the nominal values and the uncertainty
values can be obtained with functions ``nominal_values`` and
``std_devs``, respectively:

.. code:: ipython3

    from pyfreya import nominal_values, std_devs
    
    facebook.df_dau['revenue']




.. parsed-literal::

    Date
    1       210+/-23
    2       107+/-14
    3         68+/-8
    4         52+/-6
    5         43+/-5
    6         37+/-4
    7         33+/-4
    8         30+/-4
    9     27.0+/-3.3
    10    25.0+/-3.1
    11    23.3+/-2.9
    12    21.9+/-2.8
    13    20.6+/-2.6
    14    19.6+/-2.5
    15    18.6+/-2.4
    16    17.8+/-2.3
    17    17.1+/-2.2
    18    16.4+/-2.2
    19    15.8+/-2.1
    20    15.2+/-2.0
    21    14.7+/-2.0
    22    14.2+/-1.9
    23    13.8+/-1.9
    24    13.4+/-1.8
    25    13.0+/-1.8
    26    12.7+/-1.8
    27    12.4+/-1.7
    28    12.1+/-1.7
    29    11.8+/-1.7
    30    11.5+/-1.6
    31    11.2+/-1.6
    Name: revenue, dtype: object



.. code:: ipython3

    nominal_values(facebook.df_dau['revenue'])




.. parsed-literal::

    array([210.        , 107.11673044,  67.66930193,  51.72634515,
            42.74901227,  36.8733419 ,  32.67730124,  29.50438329,
            27.00601304,  24.97849563,  23.29415112,  21.86853369,
            20.64336873,  19.57703135,  18.63892795,  17.80602528,
            17.06062202,  16.38887082,  15.77976993,  15.22445827,
            14.71571191,  14.24757745,  13.81510062,  13.41412211,
            13.04112202,  12.69309974,  12.36748022,  12.06203999,
            11.77484823,  11.50421944,  11.24867512])



.. code:: ipython3

    std_devs(facebook.df_dau['revenue'])




.. parsed-literal::

    array([22.588714  , 13.73966474,  8.17827559,  6.15442801,  5.07962406,
            4.40195366,  3.9300445 ,  3.57937697,  3.3066122 ,  3.08712987,
            2.90585349,  2.75300466,  2.62194609,  2.5080031 ,  2.40778109,
            2.31875073,  2.23898552,  2.16699004,  2.1015841 ,  2.04182268,
            1.9869392 ,  1.93630468,  1.88939763,  1.84578168,  1.80508855,
            1.76700502,  1.73126286,  1.69763082,  1.66590837,  1.63592067,
            1.60751445])



It is possible to save a revenue class instance (using *pickle*) and
loading it.

.. code:: python

   facebook.save('facebook_revenue.pkl')
   facebook_loaded = pyfreya.load('facebook_revenue.pkl')
