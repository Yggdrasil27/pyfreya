Welcome to PyFreya's documentation!
===================================

.. image:: logo.png

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   installation
   usage
   tutorials
   modules
   contributing
   authors
   history

Indices and tables
==================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
