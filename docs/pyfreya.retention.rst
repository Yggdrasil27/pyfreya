pyfreya.retention package
=========================

Submodules
----------

pyfreya.retention.fit\_functions module
---------------------------------------

.. automodule:: pyfreya.retention.fit_functions
    :members:
    :undoc-members:
    :show-inheritance:

pyfreya.retention.retention module
----------------------------------

.. automodule:: pyfreya.retention.retention
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: pyfreya.retention
    :members:
    :undoc-members:
    :show-inheritance:
