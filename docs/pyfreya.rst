pyfreya package
===============

Subpackages
-----------

.. toctree::

    pyfreya.cohort
    pyfreya.retention
    pyfreya.revenue

Submodules
----------

pyfreya.pyfreya module
----------------------

.. automodule:: pyfreya.pyfreya
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: pyfreya
    :members:
    :undoc-members:
    :show-inheritance:
