=====
Usage
=====

To use PyFreya in a project::

    import pyfreya as pf

Creating a cohort is performed by defining the number of ``new_users``
and a ``days_since_install``, ``retention_values`` pair, used to fit a retention profile::

    new_users = 100
    days_since_install = [1, 7, 30]
    retention_values= [50, 15, 3]
    facebook = pf.create_cohort(new_users, days_since_install, retention_values)

For more information about retention look at :ref:`retention_example`
that shows how to pass custom functions and other features. As of right now only a simple power
function is pre-defined.

To see how well the fit was performed it can be plotted::

    facebook.plot_retention()

If this cohort comes back over a range of 10 days it should be replicated as such::

    facebook.replicate_cohort(10)

The daily active user can be plotted::

    facebook.plot_dau()

If there is an interest to see what happens over a period of time after the influx of new users
have stopped (example with 5 days)::

    facebook.replicate_cohort(10, 5)
    facebook.plot_dau()

To calculate revenue from a cohort a revenue profile can be attached.
A more complete tutorial of revenue can be found here :ref:`revenue_example`.
As of right now the only pre-defined profile is ``ARPDAU`` which simply multiplies the DAU number
on a given date with constant used in initialization::

    from pyfreya.revenue import ARPDAU
    revenue_profile = ARPDAU(1.6)
    facebook.apply_revenue(revenue_profile)
    facebook.plot_revenue()

For using ``datetime`` objects instead of simple numbers for dates pass a date::

    import datetime
    start_date = datetime.datetime.strptime('2020-01-01', '%Y-%m-%d')
    facebook = pf.create_cohort(new_users, days_since_install, retention_values, start_date=start_date)
    facebook.replicate_cohort(10, 5)
    facebook.plot_dau()

Plotting multiple cohorts uses special functions. To distinguish between them each class is given a name::

    from pyfreya import multi_cohort_dau_plot
    new_users_google = 150
    days_since_install_google = [1, 10, 20]
    retention_values_google = [50, 10, 5]
    google = pf.create_cohort(new_users, days_since_install, retention_values, name='Google')
    facebook.name = 'Facebook'
    cohorts = [facebook, google)
    multi_cohort_dau_plot(cohorts)

Multi cohort plot functions are:

 * ``multi_cohort_ret_plot``
 * ``multi_cohort_dau_plot``
 * ``multi_cohort_rev_plot``

When using uncertainties simply replace all numbers (not days since install though!!!) with a ``ufloat``
number from the `Uncertainties <https://pypi.org/project/uncertainties/>`__ package::

    from pyfreya import ufloat
    new_users_facebook = ufloat(100, 6)
    days_since_install_facebook = [1, 7, 30]
    retention_values_facebook = [ufloat(50, 2), ufloat(15, 0.5), ufloat(5, 0.1)]
    facebook = pf.create_cohort(new_users_facebook, days_since_install_facebook, retention_values_facebook, name='Facebook')
    facebook.replicate_cohort(10, 5)
    facebook.plot_dau()
